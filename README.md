# Flow

A project that i created as my final assignment to get my bachelor degree

## Getting Started

To start using this app please follow instructions below

### Prerequisites

* Clone this git to your local computer
* Install [Python 3.X](https://www.python.org/downloads/)
I'm using Python 3.7
Don't forget to add Python PATH to your environment, to make things easier

### Installing

Installing all packages required in this app

Open command prompt/terminal and move to this project directory and then install from requirements.txt
For windows user

```
pip install -r requirements.txt
```

And for linux/macOS user

```
pip3 install -r requirements.txt
```

## Running the app

To run this app you can follow the steps below
For windows user and want to use debuging feature

```
set FLASK_APP=app.py
set FLASK_DEBUG=1
flask run
```

For those who won't use debug feature simply run this

```
flask run
```

For linux/macOS user

```
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run
```

## Deployment

Still haven't tested on live server

## Built With

* [Flask](http://flask.pocoo.org/docs/1.0/) - The web framework used

## Authors

* **Demaspira Aulia** - [NoobDeDem](https://github.com/noobdedem)

## Acknowledgments

* Rizqi Hadi Prawira - [DoublesInLove](https://github.com/doublesinlove)
